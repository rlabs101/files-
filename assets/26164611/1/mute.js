var Mute = pc.createScript('mute');

// initialize code called once per entity
Mute.prototype.initialize = function() {
    this.check = false;
};

// update code called every frame
Mute.prototype.update = function(dt) {
    this.entity.element.on('click', function (event) {
        //alert('check');
        if (this.app.systems.sound.volume !== 0) {
            this.app.systems.sound.volume = 0;
            this.check = true;
        }
        
        else {
            this.app.systems.sound.volume = 1;
            this.check = false;
        }
        
    }, this);
};

// swap method called for script hot-reloading
// inherit your script state here
// Mute.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/